package ca.airspeed.demo.jmsgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JmsGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(JmsGatewayApplication.class, args);
	}
}
